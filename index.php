<?php

require_once('vendor/autoload.php');
require_once('history.php');
use MadBob\Sparqler\Client;

function printHelp($argv)
{
    echo sprintf("%s -h host [args...]\n", $argv[0]);
    echo "\n";
    echo "-h --host <endpoint>        URL of the SPARQL endpoint\n";
    echo "-u --username <username>    Optional username\n";
    echo "-p --password <password>    Optional password\n";
    echo "   --auth <digest|basic>    Optional authentication method. Accepted values: digest (default) or basic\n";
    echo "\n";
}

$shortopts = "h:u::p::";
$longopts  = ['help', 'host:', 'username::', 'password::', 'auth::'];
$options = getopt($shortopts, $longopts);

if (isset($options['help'])) {
    printHelp($argv);
    exit(0);
}

if (isset($options['h']) || isset($options['host'])) {
    $endpoint = $options['h'] ?? $options['host'];
}
else {
    printHelp($argv);
    exit(1);
}

$username = null;
$password = null;
$authtype = 'digest';

if (isset($options['u']) || isset($options['username'])) {
    $username = $options['u'] ?? $options['username'];
}

if (isset($options['p']) || isset($options['password'])) {
    $password = $options['p'] ?? $options['password'];
}

if (isset($options['auth'])) {
    $authtype = $options['auth'];
    if (in_array($authtype, ['basic', 'digest']) == false) {
        printHelp($argv);
        exit(1);
    }
}

$config = [
    'host' => $endpoint,
];

if ($username && $password) {
    $config['auth'] = [
        'type' => $authtype,
        'username' => $username,
        'password' => $password,
    ];
}

$history = new History();
$client = new Client($config);

echo "Type your SPARQL query and press Enter.\n";
echo "Type 'exit' and press Enter, or press ^C, to close the client.\n";

while (true) {
    echo "\n";
    $query = readline('> ');
    $query = trim($query);

    if ($query == '') {
        continue;
    }

    if ($query == 'exit') {
        break;
    }

    $history->pushHistory($query);

    try {
        $result = $client->query($query);
        echo $result->dump('text');
    }
    catch(\EasyRdf\Http\Exception $e) {
        echo $e->getMessage() . "\n";
        echo $e->getBody() . "\n";
    }
    catch(\Exception $e) {
        echo get_class($e) . "\n";
        echo $e->getMessage() . "\n";
    }
}
