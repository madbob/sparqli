<?php

use Xdg\BaseDirectory\XdgBaseDirectory;

class History
{
    private $cache_file;

    function __construct()
    {
        $cache_file = null;

        $platform = XdgBaseDirectory::fromEnvironment();
        $cache_folder = $platform->getCacheHome();

        if ($cache_folder) {
            $cache_folder = $cache_folder . DIRECTORY_SEPARATOR . 'sparqli';
            if (file_exists($cache_folder) == false) {
                $test = mkdir($cache_folder, 0755);
                if ($test === false) {
                    return;
                }
            }

            $cache_file = $cache_folder . DIRECTORY_SEPARATOR . 'history';
            if (file_exists($cache_file)) {
                $history = file($cache_file);

                /*
                    TODO: eventually trim cache file to latest 100 entries
                */

                foreach($history as $his) {
                    $his = trim($his);
                    readline_add_history($his);

                    /*
                        TODO: an idea is to leverage readline_completion_function
                        https://www.php.net/manual/en/function.readline-completion-function.php
                        to autocomplete most used tokens.

                        Implementation:
                        - tokenize commands from history
                        - put single tokens is an array sorted by number of occourrences
                        - in the readline_completion_function's callback, match and return tokens sorted by usage
                        - populate the sorted tokens array also dynamically, when pushing new items in history
                    */
                }
            }
        }

        $this->cache_file = $cache_file;
    }

    function pushHistory($line)
    {
        if ($this->cache_file) {
            $file = fopen($this->cache_file, 'a+');
            fwrite($file, $line . "\n");
            fclose($file);
        }

        readline_add_history($line);
    }
}
