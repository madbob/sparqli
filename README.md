# sparqli

sparqli is a (very) simple SPARQL client for CLI. Built in PHP, compiled as a PHAR using Box.

<img src="https://www.madbob.org/external/sparqli.gif" style="max-width:100%" />

- optional authentication to the SPARQL endpoint (both basic and digest HTTP auth)
- keeps permanent history of executed queries

To install it: copy the `sparqli` executable in a folder within your `$PATH`.

To build it: install [Box](https://box-project.github.io/box/) and execute `box compile` in the project's root folder.


Copyright 2023 Roberto Guido <info@madbob.org>
